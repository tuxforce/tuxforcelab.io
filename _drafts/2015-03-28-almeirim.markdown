---
layout: post
title:  "Almeirim"
author: Sebastian Schiefer
date:   2015-03-28 15:09:59
image: /img/2015.03.28.jpg
---
<h3>2015.03.28 - PARQUE ZONA NORTE, Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/e45c3d20020f4d12acf77b52009c1f5d/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/30KsfcW" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
