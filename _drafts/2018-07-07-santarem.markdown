---
layout: post
title:  "Convento de São Francisco"
author: Sebastian Schiefer
date:   2018-07-07 21:15:08
image: /img/2018.07.07b.jpg
---
<h3>2018.07.07 - Convento de São Francisco, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/5eb143610f964851a88b52a1bf0a4f74/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3b16U0i" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
