---
layout: post
title:  "São Martinho Do Porto"
author: Sebastian Schiefer
date:   2016-08-20 12:33:43
image: /img/2016.08.20a.jpg
---
<h3>2016.08.20 - São Martinho Do Porto, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/35782cf29f764d90a5271aeae8738912/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/2Sxc1is" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
