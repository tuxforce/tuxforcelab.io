---
layout: post
title:  "Mercado de natal"
author: Sebastian Schiefer
date:   2018-12-01 12:21:57
image: /img/2018.12.01.jpg
---
<h3>2018.12.01 - Mercado de natal, Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/0c9eeb7691ec4e9596807d4d581a13d0/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3de3Nnc" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
