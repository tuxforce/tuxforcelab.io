---
layout: post
title:  "Parque Zona Norte"
author: Sebastian Schiefer
date:   2003-08-24 16:51:35
image: /img/2003.08.24.A.jpg
---
<h3>2003.08.24 - Parque Zona Norte, Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/1e47fca1453847f6a5f9d6f25a103ac3/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3kVBnD0" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
