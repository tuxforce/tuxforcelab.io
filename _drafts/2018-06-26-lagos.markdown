---
layout: post
title:  "Lagos"
author: Sebastian Schiefer
date:   2018-06-26 10:44:17
image: /img/2018.06.26.jpg
---
<h3>2018.06.26 - Lagos, Algarve, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/c0d7749428e64af79ad4e5dabd32092b/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2ylpmn6" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
