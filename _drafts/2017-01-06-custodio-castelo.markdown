---
layout: post
title:  "Custódio Castelo"
author: Sebastian Schiefer
date:   2017-01-06 22:49:20
image: /img/2017.01.06b.jpg
---
<h4>2017.01.06 - Custódio Castelo, Fazendas de Almeirim, Portugal</h4>

<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Video:</h4></div>
    <div class="embed-responsive embed-responsive-16by9">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/jTL-wZaNR60" frameborder="0" allowfullscreen></iframe></div>
<div class="card-body">
    <p class="card-text">View on YouTube:</p>
    <a href="https://youtu.be/jTL-wZaNR60" class="card-link">YouTube</a>
  </div>
</div>
