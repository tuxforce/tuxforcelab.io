---
layout: post
title:  "Adega Mayor"
author: Sebastian Schiefer
date:   2015-09-26 08:57:00
image: /img/2015.09.26.jpg
---
<h3>2015.09.26 - Adega Mayor in Campo Maior, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/d63aab95c51044d58d6ea03fe67d9838/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/3e2Aepv" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
