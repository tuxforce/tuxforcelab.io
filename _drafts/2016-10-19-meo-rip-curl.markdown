---
layout: post
title:  "MEO Rip Curl Pro"
author: Sebastian Schiefer
date:   2016-10-19 10:04:27
image: /img/2016.10.19a.jpg
---
<h3>2016.10.19 - MEO Rip Curl Pro Portugal - World Surf League</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/a9bc3f9884ff44d0a2cd1597c2132195/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/2z7g8ea" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
