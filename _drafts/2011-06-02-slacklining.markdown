---
layout: post
title: "Slacklining"
author: Sebastian Schiefer
date:   2011-06-02 15:18:05
image: /img/2011.06.02.jpg
---
<h3>2011.06.02 - Slacklining, Studentendorf Efferen, Germany</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/de8bbc6f21f642d59ec9dac4bae21434/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2x6v2ki" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
