---
layout: post
title:  "Wedding"
author: Sebastian Schiefer
date:   2015-09-12 18:58:34
image: /img/2015.09.12.jpg
---
<h3>2015.09.12 - Wedding, Almeirim, Portugal</h3>

  <div class="card">
    <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
      <div class="lr_embed embed-responsive embed-responsive-16by9" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/8f7efbedef904c1694f9b254c8703e78/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
  <div class="card-body">
      <p class="card-text">overview: <a href="https://adobe.ly/37Awf1c" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
    </div>
  </div>
  <br>
  <div class="row row-cols-1 row-cols-md-2">
<div class="col mb-4">
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> The Cake Video:</h4></div>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Tgfk7ks_jyA" allowfullscreen></iframe>
  </div>
  <div class="card-body">
      <p class="card-text">View on YouTube:</p>
      <a href="https://youtu.be/Tgfk7ks_jyA" class="card-link">YouTube</a>
    </div>
  </div>
  </div>

<div class="col mb-4">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Rancho Video:</h4></div>
  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/QgIEJQNEl2U" allowfullscreen></iframe>
    </div>
    <div class="card-body">
        <p class="card-text">View on YouTube:</p>
        <a href="https://youtu.be/QgIEJQNEl2U" class="card-link">YouTube</a>
      </div>
    </div>
    </div>
