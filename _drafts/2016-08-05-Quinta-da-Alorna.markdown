---
layout: post
title:  "Quinta da Alorna"
author: Sebastian Schiefer
date:   2016-08-05 15:15:34
image: /img/2016.08.05.jpg
---
<h3>2016.08.05 - Quinta da Alorna, Almeirim, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/12c792cf90924f2aa8bcd0a5fea9fea5/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/30ESiCi" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
