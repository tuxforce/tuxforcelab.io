---
layout: post
title: "Efferino"
author: Sebastian Schiefer
date:   2010-04-04 10:18:14
image: /img/2010.04.04.jpg
---
<h3>2010.04.04 - Efferino, Studentendorf Efferen, Germany</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/bd5bd5d873f04ddfb29d0faef1b85ce7/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3aHWl2a" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
