---
layout: post
title:  "Arduino & Hacklace"
author: Sebastian Schiefer
date:   2015-12-16 06:59:00
image: /img/arduino.jpg
---
<h4>Playing some Music with my Arduino, while my Hacklace is blinking.</h4>

<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> Video:</h4></div>
<div class="embed-responsive embed-responsive-16by9">
  <video controls class="embed-responsive-item" src="{{"/video/arduino.mp4" | prepend: site.url }}"></video>
</div>
<div class="card-body">
</div>
</div>
