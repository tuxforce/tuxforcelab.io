---
layout: post
title:  "Fazendas de Almeirim"
author: Sebastian Schiefer
date:   2018-02-17 10:32:19
image: /img/2018.02.17.jpg
---
<h3>2018.02.17 - Fazendas de Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/68ed83ed3d17494b8c92269569c31ae1/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2xyXkEu" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
