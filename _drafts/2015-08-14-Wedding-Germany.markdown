---
layout: post
title:  "Wedding"
author: Sebastian Schiefer
date:   2015-08-14 17:17:08
image: /img/2015.08.14.jpg
---
<h3>2015.08.14 - Wedding, Bedburg & Bergheim, Germany</h3>

<div class="card">
  <div class="card-body">
  <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
    <div class="lr_embed embed-responsive embed-responsive-16by9" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/149101e6dc604aaa8c3559737c0915fa/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3e5j5Mb" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
<br>
<div class="row row-cols-1 row-cols-md-2">
<div class="col mb-4">
<div class="card">
<div class="card-body">
  <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Wedding in castle Bedburg:</h4></div>
<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/TSBzr47q9gM" allowfullscreen></iframe>
</div>
<div class="card-body">
    <p class="card-text">View on YouTube:</p>
    <a href="https://youtu.be/TSBzr47q9gM" class="card-link">YouTube</a>
  </div>
</div>
</div>

<div class="col mb-4">
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Wedding Party at the Restaurant "Mimosa":</h4></div>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/slcEymQwG5M" allowfullscreen></iframe>
  </div>
  <div class="card-body">
      <p class="card-text">View on YouTube:</p>
      <a href="https://youtu.be/slcEymQwG5M" class="card-link">YouTube</a>
    </div>
  </div>
  </div>
