---
layout: post
title:  "Already one year..."
author: Sebastian Schiefer
date:   2017-12-03 18:00:00
image: /img/001.jpg
---
<h3>2017.12.03 - Already one year...</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> <i class="fab my-fa-flickr" aria-hidden="true"></i> Foto:</h4></div>
<a data-flickr-embed="true" data-header="true" data-footer="true"  href="https://www.flickr.com/photos/sebastianofportugal/albums/72157689708016602" title="2017.12.03 - Almeirim, Santarém, Portugal"><img src="https://farm5.staticflickr.com/4757/39147742454_94c2716aae_z.jpg" width="100%" alt="2017.12.03 - Almeirim, Santarém, Portugal"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<div class="card-body">
    <p class="card-text">Foto on Flickr: <a href="https://www.flickr.com/photos/sebastianofportugal/albums/72157689708016602" class="card-link">Flickr Set</a></p>

    <h4>Already one year...</h4>
    <p class="card-text">Today it's already one year without you grandmother Clementina. And it would have been your 58th birthday today mom...</p>
  </div>
</div>
