---
layout: post
title:  "Festas da cidade de Almeirim"
author: Sebastian Schiefer
date:   2017-06-21 21:22:54
image: /img/2017.06.21.jpg
---
<h3>2017.06.21 - FESTAS DA CIDADE DE ALMEIRIM, Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/dec54138487c46359c75477dd7c317e7/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2z70QGi" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
