---
layout: post
title:  "Ramos e Celeste"
author: Sebastian Schiefer
date:   2018-01-12 12:39:34
image: /img/2018.01.12a.jpg
---
<h3>2018.01.12 - Restaurante Ramos e Celeste, Frade de Cima, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/ccbad0cb5d4447269606fe5f41b33248/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/3fgo1Pe" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
