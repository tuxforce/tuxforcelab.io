---
layout: post
title:  "Fazendas de Almeirim"
subtitle: ! <span class="badge badge-danger align-top">WARNING!</span><br><span>THE FOLLOWING IMAGES AND/ OR CONTENT MAY BE DISTURBING TO SOME VIEWERS. VIEWER DISCRETION IS STRONGLY ADVISED.</span>
author: Sebastian Schiefer
date:   2016-10-16 07:37:35
image: /img/2016.10.16.jpg
---
<h3>2016.10.16 - Fazendas de Almeirim, Portugal</h3>
<p class="card-text">{{ page.subtitle }}</p>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/0d12c419e1a3410daa90eff8ac413b3d/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/35sJUGs" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
