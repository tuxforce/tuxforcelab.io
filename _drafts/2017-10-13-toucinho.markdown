---
layout: post
title:  "O TOUCINHO"
author: Sebastian Schiefer
date:   2017-10-13 21:00:00
image: /img/13.10.2017.png
---
<h4>13.10.2017 - Restaurante "O TOUCINHO", Almeirim, Santarém, Portugal</h4>

<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Video:</h4></div>
    <div class="embed-responsive embed-responsive-16by9">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/FqqOisUPeYw" frameborder="0" allowfullscreen></iframe></div>
<div class="card-body">
    <p class="card-text">View on YouTube: <a href="https://youtu.be/FqqOisUPeYw" class="card-link">YouTube</a></p>
  </div>
</div>
