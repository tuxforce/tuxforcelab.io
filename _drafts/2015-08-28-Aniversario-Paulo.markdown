---
layout: post
title:  "Aniversário Paulo"
author: Sebastian Schiefer
date:   2015-08-28 21:19:18
image: /img/2015.08.28.jpg
---
<h3>2015.08.28 - Aniversário Paulo, Restaurante "O TOUCINHO", Almeirim, Portugal</h3>

<div class="row">
  <div class="col-xs-12 col-sm-6">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
  <div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/bc22b2beae734f72ad169ddb5aa213c8/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
  <div class="card-body">
      <p class="card-text">overview: <a href="https://adobe.ly/3e4KyO1" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-6">
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Video:</h4></div>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CV6YJzBcnFE" allowfullscreen></iframe>
  </div>
  <div class="card-body">
      <p class="card-text">View on YouTube:</p>
      <a href="https://youtu.be/CV6YJzBcnFE" class="card-link">YouTube</a>
    </div>
  </div>
  </div>
