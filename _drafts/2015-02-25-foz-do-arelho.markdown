---
layout: post
title: "Foz do Arelho"
author: Sebastian Schiefer
date:   2015-02-25 12:26:47
image: /img/DSC0866.jpg
---
<h3>2015.02.25 - Foz do Arelho, Leiria, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/f4c2a8e7f37a42f7b130adf9f9d517fd/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2JCORDe" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
