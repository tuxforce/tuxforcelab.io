---
layout: post
title:  "Ramos & Celeste"
author: Sebastian Schiefer
date:   2015-09-18 12:55:00
image: /img/2015.09.18.jpg
---
<h3>2015.09.18 - Ramos & Celeste in Canto Negro, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/c7fbf9ac19724a86931d0bd7ab404d93/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/3hwrjiC" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
