---
layout: post
title:  "Valentine's Day"
author: Sebastian Schiefer
date:   2017-02-14 20:30:20
image: /img/2017.02.14.png
---
<h4>2017.02.14 - Valentine's Day, Germany</h4>

<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Video:</h4></div>
    <div class="embed-responsive embed-responsive-16by9">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/pEmGLeqXdv0" frameborder="0" allowfullscreen></iframe></div>
<div class="card-body">
    <p class="card-text">View on YouTube: <a href="https://youtu.be/pEmGLeqXdv0" class="card-link">YouTube</a></p>

    <h4>Valentine's Day</h4>
    <p class="card-text">The only dates I get are updates.</p>
  </div>
</div>
