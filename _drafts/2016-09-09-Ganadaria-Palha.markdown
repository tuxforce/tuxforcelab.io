---
layout: post
title:  "Ganadaria Palha"
author: Sebastian Schiefer
date:   2016-09-09 09:39:29
image: /img/2016.09.09.jpg
---
<h3>2016.09.09 - Ganadaria Palha, Samora Correia, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/0a3cccfc49634653995b1f70298c3db9/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/3hmlJiF" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
