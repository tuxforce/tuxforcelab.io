---
layout: post
title:  "Sommerfest"
author: Sebastian Schiefer
date:   2015-06-13 18:59:38
image: /img/2015.06.13.jpg
---
<h3>2015.06.18 - Sommerfest, Studentendorf Efferen, Germany</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/35def587dab64d478989a2216c6593af/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/3eaDj7m" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
