---
layout: post
title:  "Aniversário Elias"
author: Sebastian Schiefer
date:   2017-05-27 13:02:58
image: /img/2017.05.27.jpg
---
<h3>2017.05.27 - Aniversário Elias, São José da Lamarosa, Coruche, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/ac2d6cc4962840da8bf480c1bdb24a01/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/35qfxjY" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
