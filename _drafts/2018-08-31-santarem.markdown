---
layout: post
title:  "Aniversário Margarida"
author: Sebastian Schiefer
date:   2018-08-31 20:56:16
image: /img/2018.08.31.jpg
---
<h3>2018.08.31 - Aniversário Margarida, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/b61da2079ebf4c4fb6d6949f72a3c0fd/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/2Ywd4D8" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
