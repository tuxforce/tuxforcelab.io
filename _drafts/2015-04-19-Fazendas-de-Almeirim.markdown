---
layout: post
title:  "Fazendas de Almeirim"
author: Sebastian Schiefer
date:   2015-04-19 13:19:41
image: /img/2015.04.19a.jpg
---
<h3>2015.04.19 - Fazendas de Almeirim, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/0cc3010fdda84cc8af34b4236ec21166/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">Full Screen: <a href="https://adobe.ly/3eHVn8S" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
