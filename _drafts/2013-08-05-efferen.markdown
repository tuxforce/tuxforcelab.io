---
layout: post
title: "Studentendorf Efferen"
author: Sebastian Schiefer
date:   2013-08-05 15:08:33
image: /img/2013.08.05.jpg
---
<h3>2013.08.05 - Studentendorf Efferen, Köln, Germany</h3>
<div class="row">
  <div class="col-xs-12 col-sm-6">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
  <div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/ae9322475e314952b5fc7c0370393e25/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
  <div class="card-body">
      <p class="card-text">overview: <a href="https://adobe.ly/2ZeuQd2" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-6">
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fas fa-video" aria-hidden="true"></i> <i class="fab my-fa-youtube-square" aria-hidden="true"></i> Video:</h4></div>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PVoTYxwmjJk" allowfullscreen></iframe>
  </div>
  <div class="card-body">
      <p class="card-text">View on YouTube:
      <a href="https://youtu.be/PVoTYxwmjJkw" class="card-link">YouTube</a></p>
    </div>
  </div>
  </div>
