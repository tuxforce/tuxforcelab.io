---
layout: post
title:  "Almeirim"
author: Sebastian Schiefer
date:   2002-08-02 16:00:00
image: /img/2002.08.02.jpg
---
<h3>2002.08.02 - Almeirim, Santarém, Portugal</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Fotos:</h4></div>
<div class="lr_embed" style="position: relative; padding-bottom: 50%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://lightroom.adobe.com/embed/shares/b2770d3f8b1a4133ac823717c4367b9c/slideshow?background_color=%232D2D2D&color=%23999999" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div>
<div class="card-body">
    <p class="card-text">overview: <a href="https://adobe.ly/32qEc7Z" class="card-link"><i class="fas fa-expand-arrows-alt"></i></a></p>
  </div>
</div>
