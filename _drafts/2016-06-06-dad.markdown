---
layout: post
title:  "Goodbye Mum & Dad"
author: Sebastian Schiefer
date:   2016-06-06 20:00:00
image: /img/mama&papa.jpg
---
<h3>Goodbye Mum & Dad</h3>
<div class="card">
  <div class="card-body">
    <h4 class="card-title"><i class="fa fa-camera" aria-hidden="true"></i> Foto:</h4></div>
<img src="/img/mama&papa.jpg" alt="Card image" width="100%">
<div class="card-body">
    <p class="card-text">It broke my heart to lose you, but you didn't go alone,<br>
For a big part of me went with you the day God called you home.<br><br>
I grieve for you mum & dad.<br>
It’s so hard to move on, I’m still loving what's gone.</p>
  </div>
</div>
