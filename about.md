---
layout: page
title: About me
permalink: /about/
image: /img/banner.jpg
---
<div class="row">
  <div class="col-sm-6">
  <div class="card">
<img src="/img/avatar.jpg" class="card-img" alt="Sebastian Schiefer">
<div class="card-body">
  <h5 class="card-title"><i class="fa fa-user" aria-hidden="true"></i> Sebastian Schiefer (tuxforce)</h5>
  <h6 class="card-subtitle mb-2 text-muted">Just another C8H10N4O2-Junkie <i class="fas fa-mug-hot"></i></h6>
  <p class="card-text">

  <!-- <h5><i class="fa fa-users"></i> Social Networks:</h5> -->

  <!-- Twitter -->
  <!-- <a href="https://www.twitter.com/tuxforce"><i class="fab my-fa-twitter fa-2x"></i></a> -->
  <!-- Periscope -->
<!--  <a href="https://www.pscp.tv/tuxforce"><i class="fab fa-periscope fa-2x"></i></a> -->
  <!-- Instagram -->
<!--  <a href="https://www.instagram.com/tuxf0rc3"><i class="fab my-fa-instagram fa-2x"></i></a> -->
  <!-- Snapchat trigger modal -->
<!--  <i class="fab fa-snapchat-ghost fa-2x" data-toggle="modal" data-target="#snapchat"></i> -->

<!-- Modal -->
<!-- <div class="modal fade" id="snapchat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title" id="exampleModalLabel">Snapchat QR-Code</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body"> <img src="/img/snapcode.png" class="card-img" alt="Snapchat QR-Code"> Username: tuxforce83
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>--> </p>
</div>
</div>
  </div>

<!-- Second Card -->

  <div class="col-sm-6">
  <div class="card">
<img class="card-img-top" src="/img/me2024.JPG" alt="Banner Image">
<div class="card-body">
  <!-- <h5 class="card-title"></h5> -->
  <p class="card-text">
  <i class="fas fa-birthday-cake"></i> <b>Cake day</b>:<br>
  June 16, 1983<br><br>

  <i class="fa fa-home" aria-hidden="true"></i>
  <b>Address:</b><br>
  Rua Bernardo Gonçalves.43<br>
  2080-065 <a href="http://www.cm-almeirim.pt/">Almeirim</a><br>
  Distrito de Santarém<br>
  Portugal<br><br>

 <i class="fa fa-university" aria-hidden="true"></i>
  <strong>Education:</strong><br>
   I studied <a href="https://dh.phil-fak.uni-koeln.de">Digital Humanities</a> and <a href="http://www.philosophie.uni-koeln.de">Philosophy</a> at the <a href="http://www.uni-koeln.de">University of Cologne</a>.<br><br>

  <i class="fas fa-envelope"></i>
  <strong>Email:</strong><br>
  <a href="mailto:sebastianofportugal@gmail.com">sebastianofportugal@gmail.com</a></p>
</div>
</div>
  </div>
</div>
