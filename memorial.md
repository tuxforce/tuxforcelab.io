---
layout: default
title: In memoriam
permalink: /memorial/
---
<div class="jumbotron jumbotron-fluid">
   <div class="container">
     <h1 class="display-3">{{ page.title }}</h1>
    </div>
</div>

<div class="container">

<div class="card-deck">
<div class="card" style="width: 18rem;">
  <img src="/img/inmemoriam/0003.jpg" class="card-img-top" alt="mother">
   <div class="card-img-overlay">
   <p class="card-text text-white">My Mother</p>
   </div>
  <div class="card-body">
  <h5 class="card-title">Maria da Conceição Migas da Mota Cerveira Schiefer</h5>
    <h6 class="card-subtitle mb-2 text-muted">&#42;03.12.1959 - 19.08.2000&dagger;</h6>

  </div>
</div>

<div class="card" style="width: 18rem;">
  <img src="/img/inmemoriam/0001.jpg" class="card-img-top" alt="grandparents germany">
  <div class="card-img-overlay">
  <p class="card-text text-white">My German Grandparents</p>
  </div>
  <div class="card-body">
  <h5 class="card-title">Wübine Georgina Schiefer, Geb. van Hettinga</h5>
    <h6 class="card-subtitle mb-2 text-muted">&#42;1924 - 2009&dagger;</h6>
  <h5 class="card-title">Reiner Schiefer</h5>
    <h6 class="card-subtitle mb-2 text-muted">&#42;1924 - 1992&dagger;</h6>

  </div>
</div>
</div>

<br>

<div class="card-deck">
<div class="card" style="width: 18rem;">
  <img src="/img/inmemoriam/0002.jpg" class="card-img-top" alt="grandparents portugal">
  <div class="card-img-overlay">
  <p class="card-text text-white">My Portuguese Grandparents</p>
  </div>
  <div class="card-body">
  <h5 class="card-title">Joaquim da Mota Cerveira</h5>
    <h6 class="card-subtitle mb-2 text-muted">&#42;14.12.1921 - 01.01.2015&dagger;</h6>
    <h5 class="card-title">Clementina Madaleno Migas</h5>
      <h6 class="card-subtitle mb-2 text-muted">&#42;15.06.1929 - 03.12.2016&dagger;</h6>
  </div>
</div>

<div class="card" style="width: 18rem;">
  <img src="/img/inmemoriam/0004.jpg" class="card-img-top" alt="dad">
  <div class="card-img-overlay">
  <p class="card-text text-white">My Father</p>
  </div>
  <div class="card-body">
  <h5 class="card-title">Hermann-Josef Schiefer</h5>
    <h6 class="card-subtitle mb-2 text-muted">&#42;30.03.1960 - 06.06.2016&dagger;</h6>
  </div>
</div>

</div>
